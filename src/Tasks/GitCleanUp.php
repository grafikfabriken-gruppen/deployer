<?php

namespace GF\Deployer\Tasks;

use function Deployer\task;
use function Deployer\run;
use function Deployer\writeln;
use function Deployer\before;

class GitCleanUp
{

    /**
     * Git clean up
     *
     * @param string $task_name
     * @param string $host
     * @param string $before
     */
    public function __construct($task_name, $host, $before = 'cleanup')
    {

        task($task_name, function () {
            writeln('Removing git files');
            run('cd {{release_path}} && rm -f -R .git/');
            run('cd {{release_path}} && rm -f .gitignore');
        })->onHosts(array($host));

        before($before, $task_name);
    }

}
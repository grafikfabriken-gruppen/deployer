<?php

namespace GF\Deployer\Tasks;

use function Deployer\task;
use function Deployer\after;
use function Deployer\runLocally;
use function Deployer\upload;

class BedrockUploadConstantineThemeNpm
{

    /**
     * Bedrock upload Constantine theme npm dist files
     *
     * @param string $task_name
     * @param string $host
     * @param string $after
     */
    public function __construct($task_name, $host, $after = 'deploy:vendors')
    {

        task($task_name, function () {
            runLocally('cd web/app/themes/{{theme_name}}/ && npm install && npm run {{npm_flag}}');
            upload('web/app/themes/{{theme_name}}/app/dist', '{{release_path}}/web/app/themes/{{theme_name}}/app/dist');
        })->onHosts(array($host));

        after($after, $task_name);

    }

}
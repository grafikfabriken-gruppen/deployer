<?php

namespace GF\Deployer\Tasks;

use function Deployer\task;
use function Deployer\run;
use function Deployer\writeln;
use function Deployer\before;

class FlushPermalinks
{

    /**
     * Git clean up
     *
     * @param string $task_name
     * @param string $host
     * @param string $before
     */
    public function __construct($task_name, $host, $before = 'cleanup')
    {

        task($task_name, function () {
            writeln('Flushing WordPress permalinks');
            run('cd {{release_path}} && wp rewrite flush --hard');
        })->onHosts(array($host));

        before($before, $task_name);
    }
}
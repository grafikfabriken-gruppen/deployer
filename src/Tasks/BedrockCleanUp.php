<?php

namespace GF\Deployer\Tasks;

use function Deployer\task;
use function Deployer\desc;
use function Deployer\after;
use function Deployer\run;
use function Deployer\write;
use function Deployer\writeln;

class BedrockCleanUp{

    /**
     * Bedrock Clean Up
     *
     * @param string $task_name
     * @param string $host
     * @param string $after
     */
    public function __construct($task_name, $host, $after = 'cleanup')
    {

        task($task_name, function () {
            desc('Cleaning up in theme');
            run('cd {{release_path}} rm .env.example');
            run('cd {{release_path}}/web/app/themes/{{theme_name}} && rm -rf assets');
            run('cd {{release_path}}/web/app/themes/{{theme_name}} && rm -rf node_modules');
            run('cd {{release_path}}/web/app/themes/{{theme_name}} && rm -rf bower_components');
            writeln('Removed assets files');

        })->onHosts(array($host));

        after($after, $task_name);
    }

}
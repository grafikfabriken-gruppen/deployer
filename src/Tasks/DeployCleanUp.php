<?php

namespace GF\Deployer\Tasks;

use function Deployer\task;
use function Deployer\run;
use function Deployer\writeln;
use function Deployer\before;

class DeployCleanUp
{

    /**
     * Git clean up
     *
     * @param string $task_name
     * @param string $host
     * @param string $before
     */
    public function __construct($task_name, $host, $before = 'success')
    {

        task($task_name, function () {
            writeln('Remove deploy clean up');
            run('cd {{release_path}} && rm -f deploy.php');
        })->onHosts(array($host));

        before($before, $task_name);
    }

}
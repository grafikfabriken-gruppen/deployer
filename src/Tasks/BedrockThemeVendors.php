<?php

namespace GF\Deployer\Tasks;

use function Deployer\task;
use function Deployer\desc;
use function Deployer\after;
use function Deployer\run;
use function Deployer\write;
use function Deployer\writeln;

class BedrockThemeVendors
{

    /**
     * Bedrock Clean Up
     *
     * @param string $task_name
     * @param string $host
     * @param string $after
     */
    public function __construct($task_name, $host, $after = 'deploy:vendors')
    {

        task($task_name, function () {
            writeln('Installing composer vendors in theme');
            run('cd {{release_path}}/web/app/themes/{{theme_name}} && {{bin/composer}} {{composer_options}}');
            writeln('Installed composer vendors in theme');
        })->onHosts(array($host));

        after($after, $task_name);

    }

}
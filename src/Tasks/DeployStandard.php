<?php
namespace GF\Deployer\Tasks;

use GF\Deployer\Abstracts\Singleton;
use function Deployer\desc;
use function Deployer\task;
use function Deployer\after;

class DeployStandard extends Singleton{

    public $task_name = 'deploy';

    public function _construct()
    {

        desc('Deploy your project');

        task($this->task_name, [
            'deploy:info',
            'deploy:prepare',
            'deploy:lock',
            'deploy:release',
            'deploy:update_code',
            'deploy:shared',
            'deploy:writable',
            'deploy:vendors',
            'deploy:clear_paths',
            'deploy:symlink',
            'deploy:unlock',
            'cleanup',
            'success'
        ]);

        after('deploy:failed', 'deploy:unlock');

    }

}
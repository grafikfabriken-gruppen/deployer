<?php

namespace GF\Deployer\Tasks;

use function Deployer\task;
use function Deployer\desc;
use function Deployer\after;
use function Deployer\run;
use function Deployer\write;
use function Deployer\writeln;
use function Deployer\before;
use function Deployer\upload;

class DeployToAppEngine
{

    /**
     * Bedrock Clean Up
     *
     * @param string $task_name
     * @param string $host
     * @param string $before
     */
    public function __construct($task_name, $host, $config_dir = 'gae', $before = 'success')
    {

        task($task_name, function () use($config_dir) {
            writeln('Deploying to app engine');
            upload('config/' . $config_dir . '/', '{{release_path}}');
            // run('cd {{release_path}} && rm -R .git/');
            // // run('cd {{release_path}} && rm -R vendor/');
            run('cd {{release_path}} && rm deploy.php');
            run('cd {{release_path}} && gcloud auth login');
            run('cd {{release_path}} && gcloud config set project {{gae_project_id}}');
            run('cd {{release_path}} && gcloud app deploy app.yaml cron.yaml dispatch.yaml',[
                'timeout' => 6000,
            ]);
        })->onHosts(array($host));

        before($before, $task_name);
    }

}
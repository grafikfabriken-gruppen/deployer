<?php

namespace GF\Deployer\Tasks;

use function Deployer\task;
use function Deployer\run;
use function Deployer\writeln;
use function Deployer\before;
use function Deployer\upload;

class BjurforsDeploy
{

    /**
     * Bedrock Clean Up
     *
     * @param string $task_name
     * @param string $host
     * @param string $before
     */
    public function __construct($task_name, $host, $before = 'success')
    {

        task($task_name, function () {

            writeln('Deploying API app');
            upload('config/gaeapi/', '{{release_path}}');
            run('cd {{release_path}} && rm deploy.php');

            run('cd {{release_path}} && gcloud auth login');
            run('cd {{release_path}} && gcloud config set project {{gae_project_id}}');
            run('cd {{release_path}} && gcloud app deploy app.yaml',[
                'timeout' => 6000,
            ]);



            writeln('Deploying main app');
            upload('config/gae/', '{{release_path}}');
            
            // run('cd {{release_path}} && rm -R .git/');
            // run('cd {{release_path}} && rm -R vendor/');
            
            run('cd {{release_path}} && gcloud config set project {{gae_project_id}}');
            run('cd {{release_path}} && gcloud app deploy app.yaml cron.yaml dispatch.yaml',[
                'timeout' => 6000,
            ]);
                

            
        })->onHosts(array($host));

        before($before, $task_name);
    }

}
<?php
namespace GF\Deployer\Recipies;

use GF\Deployer\Interfaces\Runner_Interface;
use GF\Deployer\Abstracts\Runner_Abstract;
use function Deployer\task;
use GF\Deployer\Tasks\DeployAppEngine;
use GF\Deployer\Tasks\BedrockUploadThemeGulp;
use GF\Deployer\Tasks\BedrockCleanUp;
use function Deployer\host;
use GF\Deployer\Tasks\BedrockThemeVendors;
use GF\Deployer\Tasks\BedrockUploadThemeNpm;
use function Deployer\localhost;
use GF\Deployer\Tasks\DeployToAppEngine;
use GF\Deployer\Tasks\GitCleanUp;
use GF\Deployer\Tasks\MDCleanUp;
use GF\Deployer\Tasks\BjurforsDeploy;

require_once 'recipe/common.php';

/**
 * This is just an example of running
 * tasks with deployer
 */
class AppEngineBjurfors
{

    /**
     * Deploy name
     *
     * @var string
     */
    public $name = 'AppEngine';

    /**
     * The git repository
     *
     * @var string
     */
    public $repository;

    /**
     * A theme name
     *
     * @var string
     */
    public $theme_name;

    /**
     * Deploy path
     *
     * @var string
     */
    public $deploy_path;

    /**
     * SSH host
     *
     * @var string
     */
    public $host = 'appengine';

    /**
     * SSH Config file
     *
     * @var string
     */
    public $config_file = '/Users/Shared/.ssh/config';

    /**
     * Stage
     *
     * @var string
     */
    public $stage = "appengine";


    /**
     * Shared files
     *
     * @var array
     */
    public $shared_files = ['.env', '.well-known'];

    /**
     * Shared dirs
     *
     * @var array
     */
    public $shared_dirs = [];


    /**
     * Are we using gulp?
     *
     * @var boolean
     */
    public $use_gulp = false;

    /**
     * Google app id
     *
     * @var string
     */
    public $project_id;

    /**
     * Construct
     *
     * @param string $_repository
     * @param string $_theme_name
     * @param string $_deploy_path
     * @param array $_shared_files
     * @param array $_shared_folders
     * @param string $_stage
     */
    public function __construct($_repository, $_theme_name, $_deploy_path, $_project_id, $_use_gulp = false, $config_dir='gae', $_shared_files = array(), $_shared_dirs = array(), $_config_file = null, $_host = null, $_stage = null)
    {   

        //Setting up variables
        $this->repository = $_repository;
        $this->theme_name = $_theme_name;
        $this->deploy_path = $_deploy_path;
        $this->config_file = is_null($_config_file) ? $this->config_file : $_config_file;
        $this->host = is_null($_host) ? $this->host : $_host;
        $this->shared_files = count($_shared_files) > 0 ? $_shared_files : $this->shared_files;
        $this->shared_dirs = count($_shared_dirs) > 0 ? $_shared_dirs : $this->shared_dirs;
        $this->use_gulp = $_use_gulp;
        $this->project_id = $_project_id;

        //Add deployment task
        DeployAppEngine::getInstance();

        //Add other tasks!
        // new BedrockCleanUp('bedrock_clean_up', $this->host);
        new BedrockThemeVendors('bedrock_theme_vendors', $this->host, 'deploy:writable');
        new GitCleanUp('git_cleanup_appengine', $this->host);
        new MDCleanUp('md_clean_up_appengine', $this->host);
        new BjurforsDeploy('deploy_to_appengine', $this->host);

        // if ($this->use_gulp) {
        //     new BedrockUploadThemeGulp('theme_upload_gulp', $this->host, 'deploy:writable');
        // } else {
        //     new BedrockUploadThemeNpm('theme_upload_npm', $this->host, 'deploy:writable');
        // }

        //Set host
        $this->set_host();

    }

    /**
     * Local
     */
    public function set_host()
    {
        localhost($this->host)
            ->stage($this->stage)
            ->configFile($this->config_file)
            ->set('repository', $this->repository)
            ->set('deploy_path', $this->deploy_path)
            ->set('gae_project_id', $this->project_id)
            ->set('git_tty', true)
            ->set('theme_name', $this->theme_name)
            ->set('keep_releases', 10)
            ->set('npm_flag', 'production')
            ->set('composer_options', 'clearcache && {{bin/composer}} install --no-dev')
            ->set('shared_files', $this->shared_files)
            ->set('shared_dirs', $this->shared_dirs)
            ->set('writable_dirs', [])
            ->set('allow_anonymous_stats', false);
    }

}
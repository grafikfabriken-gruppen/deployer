<?php
namespace GF\Deployer\Recipies;

use GF\Deployer\Interfaces\Runner_Interface;
use GF\Deployer\Abstracts\Runner_Abstract;
use function Deployer\task;
use function Deployer\localhost;
use GF\Deployer\Tasks\DeployStandard;
use GF\Deployer\Tasks\BedrockUploadThemeGulp;
use GF\Deployer\Tasks\BedrockCleanUp;
use function Deployer\host;
use GF\Deployer\Tasks\BedrockThemeVendors;
use GF\Deployer\Tasks\BedrockUploadThemeNpm;

require_once 'recipe/common.php';

/**
 * This is just an example of running
 * tasks with deployer
 */
class Localhost
{

    /**
     * Deploy name
     *
     * @var string
     */
    public $name = 'Localhost';

    /**
     * The git repository
     *
     * @var string
     */
    public $repository;

    /**
     * A theme name
     *
     * @var string
     */
    public $theme_name;

    /**
     * Deploy path
     *
     * @var string
     */
    public $deploy_path;

    /**
     * SSH host
     *
     * @var string
     */
    public $host = 'localhost';

    /**
     * SSH Config file
     *
     * @var string
     */
    public $config_file = '/Users/Shared/.ssh/config';

    /**
     * Stage
     *
     * @var string
     */
    public $stage = "localhost";


    /**
     * Shared files
     *
     * @var array
     */
    public $shared_files = ['.env', '.well-known'];

    /**
     * Shared dirs
     *
     * @var array
     */
    public $shared_dirs = ['web/app/uploads'];


    /**
     * Are we using gulp?
     *
     * @var boolean
     */
    public $use_gulp = false;

    /**
     * Construct
     *
     * @param string $_repository
     * @param string $_theme_name
     * @param string $_deploy_path
     * @param array $_shared_files
     * @param array $_shared_folders
     * @param string $_stage
     */
    public function __construct($_repository, $_theme_name, $_deploy_path, $_use_gulp = false, $_shared_files = array(), $_shared_dirs = array(), $_config_file = null, $_host = null, $_stage = null) 
    {   

        //Setting up variables
        $this->repository = $_repository;
        $this->theme_name = $_theme_name;
        $this->deploy_path = $_deploy_path;
        $this->config_file = is_null($_config_file) ? $this->config_file : $_config_file;
        $this->host = is_null($_host) ? $this->host : $_host;
        $this->shared_files = count($_shared_files) > 0 ? $_shared_files : $this->shared_files;
        $this->shared_dirs = count($_shared_dirs) > 0 ? $_shared_dirs : $this->shared_dirs;
        $this->use_gulp = $_use_gulp;

        //Add deployment task
        DeployStandard::getInstance();

        //Add other tasks!
        new BedrockCleanUp('bedrock_clean_up', $this->host);
        new BedrockThemeVendors('bedrock_theme_vendors', $this->host);

        if($this->use_gulp){
            new BedrockUploadThemeGulp('theme_upload_gulp', $this->host);
        }else{
            new BedrockUploadThemeNpm('theme_upload_npm', $this->host);
        }
        

        //Set host
        $this->set_host();

    }

    /**
     * Local
     */
    public function set_host()
    {

        localhost($this->host)
            ->stage($this->stage)
            ->configFile($this->config_file)
            ->set('repository', $this->repository)
            ->set('deploy_path', $this->deploy_path)
            ->set('git_tty', true)
            ->set('theme_name', $this->theme_name)
            ->set('keep_releases', 10)
            ->set('npm_flag', 'production')
            ->set('composer_options', 'clearcache && {{bin/composer}} install --no-dev')
            ->set('shared_files', $this->shared_files)
            ->set('shared_dirs', $this->shared_dirs)
            ->set('writable_dirs', [])
            ->set('allow_anonymous_stats', false);

    }

}